#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define XSIZE 21
#define YSIZE 21
#define MAX_DISPLACE 10

int terrain[XSIZE][YSIZE];

int rand_point()
{
    return rand() % 100 - 15;
}

int sum_point(int a, int b, int c, int d)
{
    return (a + b + c + d) / 4 + (rand() % MAX_DISPLACE * 2 - MAX_DISPLACE);
}

void diamond_square(int *terrain, int cols,
                    int x1, int y1, int x2, int y2)
{
    int midx = (x2 - x1) / 2 + x1;
    int midy = (y2 - y1) / 2 + y1;

    /* diamond */
    terrain[midy*cols+midx] = sum_point(
            terrain[y1*cols+x1],
            terrain[y1*cols+x2],
            terrain[y2*cols+x2],
            terrain[y2*cols+x2]);

    /* square */
    terrain[midy*cols+x1] = sum_point(
            terrain[y1*cols+x1],
            terrain[y2*cols+x1],
            terrain[midy*cols+midx],
            terrain[midy*cols+midx]);
    terrain[y1*cols+midx] = sum_point(
            terrain[y1*cols+x1],
            terrain[y1*cols+x2],
            terrain[midy*cols+midx],
            terrain[midy*cols+midx]);
    terrain[midy*cols+x2] = sum_point(
            terrain[y1*cols+x2],
            terrain[y2*cols+x2],
            terrain[midy*cols+midx],
            terrain[midy*cols+midx]);
    terrain[y2*cols+midx] = sum_point(
            terrain[y2*cols+x1],
            terrain[y2*cols+x2],
            terrain[midy*cols+midx],
            terrain[midy*cols+midx]);

}

int main(void)
{
    int *terrain;
    int x = XSIZE-1;
    int y = YSIZE-1;

    srand(time(NULL));

    if (!(terrain = calloc(XSIZE * YSIZE, sizeof(int))))
        exit(1);

    /* starting square */
    terrain[0] = rand_point();
    terrain[x] = rand_point();
    terrain[y*XSIZE] = rand_point();
    terrain[y*XSIZE+x] = rand_point();

    while (x > 0 && y > 0) {
        diamond_square(terrain, XSIZE, 0, 0, x, y);
        x/=2;
        y/=2;
    }

    for (int x=0; x<XSIZE; x++) {
        for (int y=0; y<YSIZE; y++)
            printf("%4d", terrain[y + (x * YSIZE)]);
        printf("\n");
    }

    free(terrain);

    return 0;
}
